use std::fs::File;
use std::io::{self,BufRead};

struct Submarine {
    pos: u32,
    depth: u32,
    aim: i32
}

impl Submarine {
    fn new() -> Submarine {
        Submarine { pos: 0, depth: 0, aim: 0 }
    }
    
    fn down(&mut self, n: u32) {
        self.aim += n as i32;
    }
    
    fn up(&mut self, n: u32) {
        self.aim -= n as i32;
    }
    
    fn forward(&mut self, n: u32) {
        self.pos += n;
        self.depth += (self.aim * n as i32) as u32
    }

}

fn main() {
    let file = File::open("day02_2/input").unwrap();
    let lines = io::BufReader::new(file).lines();
    let mut sub = Submarine::new();
    
    for line in lines {
        if let Ok(l) = line {
            let mut tbl = l.split(" ").into_iter();
            let cmd = tbl.next().unwrap();
            let n : u32 = tbl.next().unwrap().parse().unwrap();
            match cmd {
                "forward" => { sub.forward(n); }
                "up" => { sub.up(n); }
                "down" => { sub.down(n); }
                _ => panic!("unknown command")
            }
        }
    }
    println!("{}", sub.pos * sub.depth);
}
