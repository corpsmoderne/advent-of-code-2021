use std::fs::File;
use std::io::{self,BufRead};

fn main() {
    let file = File::open("day07_1/input").unwrap();
    let mut lines = io::BufReader::new(file).lines();
    let line = lines.next().expect("bad input").expect("bad input");
    let subs : Vec<usize> = line.split(',')
	.map(|s| s.parse().expect("not a number"))
	.collect();
    
    let len = subs.len();
    let mut sorted = subs.clone();
    sorted.sort();
    let best = sorted[len/2];
    
    let mut fuel : i32 = 0;
    for sub in subs {
	fuel += (best as i32 - sub as i32).abs();
    }
    println!("best: {} ; fuel: {}", best, fuel);
}
