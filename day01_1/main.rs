use std::fs::File;
use std::io::{self,BufRead};

fn main() {
    let file = File::open("day01_1/input").unwrap();
    let lines = io::BufReader::new(file).lines();
    let mut last : Option<u32> = None;
    let mut count = 0;
    
    for line in lines {
        if let Some(n) = line.ok().and_then(|s| s.parse::<u32>().ok()) {
            if let Some(n_ast) = last {
                if n_ast < n {
                    count += 1;
                }
            }
            last = Some(n);
        }
    }
    println!("{}", count);
}
