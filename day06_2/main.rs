use std::fs::File;
use std::io::{self,BufRead};
use std::collections::VecDeque;

const DAYS : usize = 256;

fn main() {
    let file = File::open("day06_2/input").unwrap();
    let mut lines = io::BufReader::new(file).lines();
    let line = lines.next().expect("bad input").expect("bad input");
    let fishes : Vec<usize> = line.split(',')
	.map(|s| s.parse().expect("not a number"))
	.collect();

    let mut sea = VecDeque::from(vec![0; 9]);
    for fish in fishes {
	sea[fish] += 1;
    }

    for _i in 1..=DAYS {
	let new_borns = sea.pop_front().expect("this should not fail :(");
	sea.push_back(new_borns);
	sea[6] += new_borns;
    }
    println!("there are {} fishes after {} days ",
	     sea.iter().sum::<usize>(), DAYS);
}
