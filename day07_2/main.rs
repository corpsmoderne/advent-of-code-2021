use std::fs::File;
use std::io::{self,BufRead};
//use std::collections::HashMap;

fn main() {
    let file = File::open("day07_2/input").unwrap();
    let mut lines = io::BufReader::new(file).lines();
    let line = lines.next().expect("bad input").expect("bad input");
    let subs : Vec<i32> = line.split(',')
	.map(|s| s.parse().expect("not a number"))
	.collect();
    let max = subs.iter().max().expect("i need a maximum :(") + 1;
    let costs = calc_costs(&subs, max);
    
    let mut best_idx = max;
    let mut best_cost = costs.iter().max().expect("i need a maximum :(");
    for (i,cost) in costs.iter().enumerate() {
	if best_cost > cost {
	    best_cost = cost;
	    best_idx = i as i32;
	}
    }
    
    println!("best_cost: {best_cost} ; best_idx: {best_idx}");
}

fn calc_costs(subs: &[i32], max: i32) -> Vec<u32> {
    let mut costs = vec![0 ; max as usize];

    for sub in subs {
	for i in 0..max {
	    let dist = (sub-i).abs();
	    costs[i as usize] += calc_cost(dist);
	}
    }
    costs
}

fn calc_cost(dist: i32) -> u32 {
    let mut cost = 0;
    let mut cost_rate = 1;
    for _ in 0..dist {
	cost += cost_rate;
	cost_rate += 1;
    }
    cost
}
