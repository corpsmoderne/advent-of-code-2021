use std::fs::File;
use std::io::{self,BufRead};

fn main() {
    let file = File::open("day01_2/input").unwrap();
    let lines : Vec<u32>= io::BufReader::new(file).lines()
        .into_iter()
        .map(|l| l.unwrap().parse::<u32>().unwrap())
        .collect();
    let mut last : Option<u32> = None;
    let mut count = 0;
    
    for ((a,b),c) in lines.iter().zip(&lines[1..]).zip(&lines[2..]) {
        let sum = a+b+c;
        if let Some(last_sum) = last {
            if last_sum < sum {
                count += 1;
            }
        }
        last = Some(sum);
    }
    println!("{}", count);
}
