use std::fs::File;
use std::io::{self,BufRead,BufReader,Lines};
use std::collections::HashMap;

#[derive(Debug,Clone,Copy,Eq, Hash, PartialEq)]
struct Point {
    x: u16,
    y: u16
}
#[derive(Debug,Clone,Copy)]
struct Line {
    from: Point,
    to: Point
}

fn main() {
    let file = File::open("day05_1/input").unwrap();
    let lines = io::BufReader::new(file).lines();
    let vents = get_vents(lines);
    let map = gen_map(&vents);

    // print_map(&map);
    
    let dangers: Vec<((u16,u16),u16)> = map.into_iter()
	.filter(|(_,n)| *n >= 2)
	.collect();
    
    println!("dangerous points = {}", dangers.len());
}

fn gen_map(vents: &Vec<Line>) -> HashMap<(u16,u16),u16> {
    let mut map : HashMap<(u16,u16), u16> = HashMap::new();

    for line in vents {
	if line.from.x == line.to.x {
	    let y0 = line.from.y.min(line.to.y);
	    let y1 = line.from.y.max(line.to.y);	    
	    for y in y0..=y1 {
		update_map(&mut map, &(line.from.x, y));
	    }
	}
	if line.from.y == line.to.y {
	    let x0 = line.from.x.min(line.to.x);
	    let x1 = line.from.x.max(line.to.x);	    
	    for x in x0..=x1 {
		update_map(&mut map, &(x, line.from.y));
	    }
	}
	println!("{:?}", line);
    }
    map
}

fn update_map(map: &mut HashMap<(u16,u16),u16>, pt: &(u16,u16)) {
    let x = pt.0;
    let y = pt.1;
	
    if map.contains_key(&(x,y)) {
	let p = map.get_mut(&(x,y)).unwrap();
	*p += 1;
    } else {
	map.insert((x,y), 1);
    }
}

/*
fn print_map(map: &HashMap<(u16,u16),u16>) {
    for y in 0..10 {
	for x in 0..10 {
	    print!("{}", if let Some(n) = map.get(&(x,y)) {
		format!("{} ", n)
	    } else {
		". ".to_string()
	    });
	}
	println!();
    }
}
*/  

fn get_vents(lines: Lines<BufReader<File>>) -> Vec<Line> {
    lines.into_iter().map(|l| {
	let line = l.expect("bad input");
	let tbl : Vec<Point> = line.split(" -> ")
	    .into_iter()
	    .map(|s| {
		let pts : Vec<u16> = s.split(",")
		    .map(|w| w.parse().expect("not a number"))
		    .collect();
		match &pts[..] {
		    [x, y] => Point { x: *x, y: *y },
		    _ => panic!("bad input")
		}
	    })
	    .collect();
	match &tbl[..] {
	    [from, to] => Line { from: *from, to: *to },
	    _ => panic!("bad input")
	}
    }).collect()
}
