use std::fs::File;
use std::io::{self,BufRead,BufReader,Lines};
use std::collections::HashMap;
use std::ops::AddAssign;
    
#[derive(Debug,Clone,Copy,Eq, Hash, PartialEq)]
struct Point {
    x: i32,
    y: i32
}

impl Point {
    fn new(x: i32, y: i32) -> Self {
	Point { x, y }
    }
}

impl AddAssign for Point {
    fn add_assign(&mut self, rhs: Self) {
	self.x += rhs.x;
	self.y += rhs.y;
    }
}

#[derive(Debug,Clone,Copy)]
struct Line {
    from: Point,
    to: Point
}

type Map = HashMap<Point,u32>;

fn main() {
    let file = File::open("day05_1/input").unwrap();
    let lines = io::BufReader::new(file).lines();
    let vents = get_vents(lines);
    let map = gen_map(&vents);

    // print_map(&map);

    let dangerous_points_count = map.into_iter()
	.filter(|(_,n)| *n >= 2)
	.count();
    
    println!("dangerous points = {}", dangerous_points_count);
}

fn gen_map(vents: &Vec<Line>) -> Map {
    let mut map : Map = HashMap::new();

    for line in vents {
	update_map_line(&mut map, line);
    }
    map
}

fn update_map_line(map: &mut Map, line: &Line) {
    let mut pt = line.from;
    let pv = Point::new(line.to.x - line.from.x, line.to.y - line.from.y);
    let ps = Point::new(pv.x.signum(), pv.y.signum());
    let diff = pv.x.abs().max(pv.y.abs());
    
    for _ in 0..=diff {
	update_map_point(map, &pt);
	pt += ps; 
    }
}

fn update_map_point(map: &mut Map, pt: &Point) {
    if map.contains_key(pt) {
	let p = map.get_mut(pt).unwrap();
	*p += 1;
    } else {
	map.insert(*pt, 1);
    }
}

/*
fn print_map(map: &Map) {
    for y in 0..10 {
	for x in 0..10 {
	    print!("{}", if let Some(n) = map.get(&Point::new(x,y)) {
		format!("{}", n)
	    } else {
		".".to_string()
	    });
	}
	println!();
    }
}
*/

fn get_vents(lines: Lines<BufReader<File>>) -> Vec<Line> {
    lines.into_iter().map(|l| {
	let line = l.expect("bad input");
	let tbl : Vec<Point> = line.split(" -> ")
	    .into_iter()
	    .map(|s| {
		let pts : Vec<i32> = s.split(',')
		    .map(|w| w.parse().expect("not a number"))
		    .collect();
		match &pts[..] {
		    [x, y] => Point { x: *x, y: *y },
		    _ => panic!("bad input")
		}
	    })
	    .collect();
	match &tbl[..] {
	    [from, to] => Line { from: *from, to: *to },
	    _ => panic!("bad input")
	}
    }).collect()
}
