use std::fs::File;
use std::io::{self,BufRead};

fn main() {
    let file = File::open("day02_1/input").unwrap();
    let lines = io::BufReader::new(file).lines();
    let mut pos=0;
    let mut depth=0;
    
    for line in lines {
        if let Ok(l) = line {
            let mut tbl = l.split(" ").into_iter();
            let cmd = tbl.next().unwrap();
            let n : u32 = tbl.next().unwrap().parse().unwrap();
            match cmd {
                "forward" => { pos += n; },
                "up" => { depth -= n; },
                "down" => { depth += n; }
                _ => panic!("unknown command")
            }
        }
    }
    println!("{}", pos*depth);
}
