use std::fs::File;
use std::io::{self,BufRead};

fn main() {
    let data = get_data("day03_2/input");
    let ox = find_num(data.clone(), true);
    let co2 = find_num(data.clone(), false);
    println!("=> ox:{:?}, co2:{:?}", ox, co2);
    println!("res: {}", ox as u32 * co2 as u32);
}

fn find_num(mut data: Vec<Vec<bool>>, keep: bool) -> u16 {
    let n = data[0].len();
    for i in 0..n {
	let tops = find_tops(&data, i);
	let data2 = data.into_iter().filter(|v| {
	    if keep { v[i] == tops } else { v[i] != tops }
	}).collect();
	data = data2;
	if data.len() == 1 {
	    break;
	}
    }
    vec_to_num(&data[0])
}

fn find_tops(data: &Vec<Vec<bool>>, i: usize) -> bool {
    let mut stats : (u32, u32) = (0,0);

    for v in data {
	if v[i] {
	    stats.1 += 1;
	} else {
	    stats.0 += 1;
	}
    };
    if stats.1 >= stats.0 { true } else { false }
}

fn vec_to_num(v: &Vec<bool>) -> u16 {
    let mut gamma : u16 = 0;

    for (i, b) in v.iter().rev().enumerate() {
        gamma += (if *b { 1 } else { 0 }) << i;
    }
    gamma
}

fn get_data(file: &str) -> Vec<Vec<bool>> {
    let file = File::open(file).unwrap();
    let lines = io::BufReader::new(file).lines();
    let mut data : Vec<Vec<bool>> = Vec::new();
    
    for optline in lines {
	let line = optline.expect("bad input");
	let mut v = Vec::new();
	for c in line.chars() {
	    match c {
		'0' => v.push(false),
		'1' => v.push(true),
		_ => panic!("bad character in file")
	    }
	}
	data.push(v);
    }
    data
}
