use std::fs::File;
use std::io::{self,BufRead};

const DAYS : usize = 80;

fn main() {
    let file = File::open("day06_1/input").unwrap();
    let mut lines = io::BufReader::new(file).lines();
    let line = lines.next().expect("bad input").expect("bad input");
    let mut fishes : Vec<u8> = line.split(',')
	.map(|s| s.parse().expect("not a number"))
	.collect();

    for _i in 1..=DAYS {
	let mut new_borns=0;    
	for fish in fishes.iter_mut() {
	    if *fish == 0 {
		*fish = 6;
		new_borns += 1;
	    } else {
		*fish -= 1;
	    }
	}
	let mut tmp = vec![8; new_borns];
	fishes.append(&mut tmp);
    }
    println!("there are {} fishes after {} days ", fishes.len(), DAYS);
}
