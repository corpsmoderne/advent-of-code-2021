use std::fs::File;
use std::io::{self,BufRead};

fn main() {
    let stats = get_stats("day03_1/input");
    let (gamma, epsilon) = stat_to_nbrs(&stats);
    println!("{}", gamma as u32 * epsilon as u32)
}

fn get_stats(file: &str) -> Vec<(u32,u32)> {
    let file = File::open(file).unwrap();
    let lines = io::BufReader::new(file).lines();
    let mut stats : Vec<(u32,u32)> = vec!((0,0); 12);
    
    for optline in lines {
	let line = optline.expect("bad input");
	for (i,c) in line.chars().enumerate() {
	    match c {
		'0' => stats[i].0 += 1,
		'1' => stats[i].1 += 1,
		_ => panic!("bad character in file")
	    }
	}
    }
    stats
}

fn stat_to_nbrs(stats: &[(u32,u32)]) -> (u16,u16) {
    let mut gamma : u16 = 0;
    let mut epsilon : u16 = 0;
    
    for (i, (zeroes, ones)) in stats.iter().rev().enumerate() {
	gamma += (if zeroes > ones { 0 } else { 1 }) << i;
	epsilon += (if zeroes > ones { 1 } else { 0 }) << i;
    }
    (gamma, epsilon)
}
