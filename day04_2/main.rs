use std::fs::File;
use std::io::{self,BufRead,BufReader,Lines};
use std::collections::HashMap;

#[derive(Debug)]
struct Board {
    nums: HashMap<u16, (usize,usize)>,
    drawns: HashMap<(usize,usize), u16>
}

impl Board {
    fn add(&mut self, v: &u16) {
	if let Some((x,y)) = self.nums.get(v) {
	    self.drawns.insert((*x, *y), *v);
	}
    }
    
    fn wins(&self) -> bool {
	for x in 0..5 {
	    let mut l = vec![];
	    let mut c = vec![];
	    for y in 0..5 {
		if let Some(v) = self.drawns.get(&(x,y)) {
		    l.push(v);
		}
		if let Some(v) = self.drawns.get(&(y,x)) {
		    c.push(v);
		}		
	    }
	    if l.len() == 5 || c.len() == 5 {
		return true;
	    }
	}
	false
    }

    fn get_score(&self) -> u32 {
	let mut score : u32 = 0;

	for (n,coord) in self.nums.iter() {
	    if !self.drawns.contains_key(coord) {
		score += *n as u32;
	    }
	}
	score
    }
}

fn main() {
    let file = File::open("day04_2/input").unwrap();
    let mut lines = io::BufReader::new(file).lines();
    
    let seq_line = lines.next().expect("not enough lines").expect("wut?");
    let seq = get_seq(seq_line);
    
    println!("seq => {:?}", seq);
    let mut boards = Vec::new();
    
    while eat_empty_line(&mut lines) {
	boards.push(get_board(&mut lines));
    }
    
    for v in seq {
	println!("draw {v}");
	for b in boards.iter_mut() {
	    b.add(&v);
	}
	if boards.len() == 1 && boards[0].wins() {
	    println!("the last board to win has score: {}",
		     v as u32 * boards[0].get_score());
	    return;
	}	
	let new_boards = boards.into_iter().filter(|b| !b.wins()).collect();
	boards = new_boards;
    }
}

fn get_seq(line: String) -> Vec<u16> {
    line.split(",").into_iter()
	.map(|s| s.parse().expect("not a number"))
	.collect()
}

fn eat_empty_line(lines: &mut Lines<BufReader<File>>) -> bool {
    if let Some(Ok(l)) = lines.next() {
	if l.len() != 0 {
	    panic!("line should be empty: {}", l);
	} else {
	    true
	}
    } else {
	false
    }
}

fn get_board(lines: &mut Lines<BufReader<File>>) -> Board { //Vec<Vec<u16>> {
    let v : Vec<Vec<u16>> = (0..5).map(|_| {
	let l = lines.next().expect("not enough lines").expect("wut?");
	let tbl: Vec<u16> = l.split(" ").into_iter()
	    .filter(|s| s.len() > 0)
	    .map(|s| s.parse().expect("not a number"))
	    .collect();
	if tbl.len() != 5 {
	    panic!("bad board format!");
	}
	tbl
    }).collect();
    let mut h : HashMap<u16,(usize,usize)> = HashMap::new();
    for (i,l) in v.iter().enumerate() {
	for (j,n) in l.iter().enumerate() {
	    h.insert(*n, (i,j));
	}
    }
    Board { nums: h, drawns: HashMap::new() }
}



